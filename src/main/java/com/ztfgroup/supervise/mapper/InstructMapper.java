package com.ztfgroup.supervise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ztfgroup.supervise.entity.Instruct;
import com.ztfgroup.supervise.entity.vo.InstructVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 指示表 Mapper 接口
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
public interface InstructMapper extends BaseMapper<Instruct> {

    /**
     * 以项目板块CATEGORY_ID  部门DEPT_ID  用户USER_ID  查询获取历史指示信息
     * @param categoryId 项目板块CATEGORY_ID
     * @param deptId 部门DEPT_ID
     * @param userId 用户USER_ID
     * @return 指示信息
     */
    List<InstructVO> findAllInstruct(@Param(value = "categoryId") String categoryId, @Param(value = "deptId") String deptId, @Param(value = "userId") String userId, @Param("startTime") String startTime, @Param("endTime") String endTime);


    /**
     * 新增指示
     * @param instruct
     * @return
     */
    Integer addInstruct(Instruct instruct);


    /**
     * 根据指示ID获取指示信息及对应指示的人员
     * @param instructId
     * @return
     */
    InstructVO findInstructAndUserNameById(@Param("instructId") String instructId);
}
