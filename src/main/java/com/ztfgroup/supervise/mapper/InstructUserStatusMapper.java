package com.ztfgroup.supervise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ztfgroup.supervise.entity.InstructUserStatus;

/**
 * <p>
 * 指示用户记录表 Mapper 接口
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
public interface InstructUserStatusMapper extends BaseMapper<InstructUserStatus> {


    /**
     * 新增指示已读记录
     * @param instructUserStatus
     * @return
     */
    Integer addInstructUserStatus(InstructUserStatus instructUserStatus);
}
