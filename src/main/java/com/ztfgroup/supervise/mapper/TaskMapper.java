package com.ztfgroup.supervise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ztfgroup.supervise.entity.Task;
import com.ztfgroup.supervise.entity.vo.CategoryVO;
import com.ztfgroup.supervise.entity.vo.TaskVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 任务清单表 Mapper 接口
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
public interface TaskMapper extends BaseMapper<Task> {

    /**
     * 以当前用户USER_ID  选择用户USER_ID  项目板块CATEGORY_ID  部门DEPT_ID查询其所能见的任务
     * @param selfUserId 当前用户USER_ID
     * @param otherUserId 选择用户USER_ID
     * @param categoryId 项目板块CATEGORY_ID
     * @param deptId 部门DEPT_ID
     * @return 任务
     */
    List<TaskVO> findTaskByUserIdCategoryIdDeptId(@Param(value = "selfUserId") String selfUserId, @Param(value = "otherUserId") String otherUserId, @Param(value = "categoryId") String categoryId, @Param(value = "deptId") String deptId);




    TaskVO findTaskByTaskId(@Param(value = "taskId") String taskId);
}
