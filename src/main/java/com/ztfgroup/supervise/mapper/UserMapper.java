package com.ztfgroup.supervise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ztfgroup.supervise.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 人员表 Mapper 接口
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

    /**
     * 以当前用户USER_ID  项目板块CATEGORY_ID  部门DEPT_ID查询其所能见的其他用户信息
     * @param userId 用户USER_ID
     * @param categoryId 项目板块CATEGORY_ID
     * @param deptId 部门DEPT_ID
     * @return 用户信息
     */
    List<User> findTaskUser(@Param("userId") String userId, @Param("categoryId") String categoryId, @Param("deptId") String deptId);

    /**
     * 以项目板块CATEGORY_ID  部门DEPT_ID查询其所能见的其他用户信息
     * @param categoryIds 项目板块CATEGORY_ID
     * @param deptIds 部门DEPT_ID
     * @return 用户信息
     */
    List<User> findAllUser(@Param("categoryIds") String categoryIds, @Param("deptIds") String deptIds);


    /**
     * 根据用户ID获取微信ID
     * @param userIds
     * @return
     */
    List<User> findUserById(@Param("userIds") String userIds);


    /**
     * 根据人员ID获取所属角色
     * @param userId
     * @return
     */
    User findRoleByUserId(@Param("userId") String userId);
}
