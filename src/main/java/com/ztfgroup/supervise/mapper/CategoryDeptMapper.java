package com.ztfgroup.supervise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ztfgroup.supervise.entity.CategoryDept;


/**
 * <p>
 * 项目板块部门表 Mapper 接口
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
public interface CategoryDeptMapper extends BaseMapper<CategoryDept> {

}
