package com.ztfgroup.supervise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ztfgroup.supervise.entity.Dept;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
public interface DeptMapper extends BaseMapper<Dept> {

    /**
     * 以当前用户USER_ID  项目板块CATEGORY_ID查询其所能见的部门
     * @param userId 用户USER_ID
     * @param categoryId 项目板块CATEGORY_ID
     * @return 部门
     */
    List<Dept> findTaskDept(@Param("userId") String userId, @Param("categoryId") String categoryId);

    /**
     * 以项目板块CATEGORY_ID查询其所能见的部门
     * @param categoryIds 项目板块CATEGORY_ID
     * @return 部门
     */
    List<Dept> findAllDept(@Param("categoryIds")String categoryIds);
}
