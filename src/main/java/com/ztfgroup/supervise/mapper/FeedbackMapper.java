package com.ztfgroup.supervise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ztfgroup.supervise.entity.Feedback;
import com.ztfgroup.supervise.entity.vo.FeedbackVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 任务跟踪表 Mapper 接口
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */

public interface FeedbackMapper extends BaseMapper<Feedback> {

    List<FeedbackVO> findFeedbackByTaskId(@Param(value = "taskId") String taskId);

    Integer addFeeback(Feedback feedback);

    Feedback selectFeedbackById(String feedbackId);

}
