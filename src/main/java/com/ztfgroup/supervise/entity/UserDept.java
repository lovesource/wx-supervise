package com.ztfgroup.supervise.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 人员部门表
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
@TableName("TB_USER_DEPT")
@Data
@ToString
public class UserDept extends Model<UserDept> {

    private static final long serialVersionUID = 1L;

    /**
     * 人员ID
     */
    @TableField("USER_ID")
    private String userId;

    /**
     * 部门ID
     */
    @TableField("DEPT_ID")
    private String deptId;

    @Override
    protected Serializable pkVal() {
        return null;
    }
    
}
