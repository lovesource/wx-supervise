package com.ztfgroup.supervise.entity.vo;

import com.ztfgroup.supervise.entity.Instruct;
import com.ztfgroup.supervise.entity.User;
import lombok.Data;

import java.util.List;

@Data
public class InstructVO extends Instruct {

    private List<User> userList;
    private List<UserInstructVO> userInstructVOList;
}
