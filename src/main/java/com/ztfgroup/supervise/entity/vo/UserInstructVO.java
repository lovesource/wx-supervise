package com.ztfgroup.supervise.entity.vo;

import com.ztfgroup.supervise.entity.InstructUserStatus;
import com.ztfgroup.supervise.entity.User;
import lombok.Data;

@Data
public class UserInstructVO extends User {

    private InstructUserStatus instructUserStatus;
}
