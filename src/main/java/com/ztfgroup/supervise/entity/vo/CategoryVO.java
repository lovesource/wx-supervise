package com.ztfgroup.supervise.entity.vo;

import com.ztfgroup.supervise.entity.Category;
import com.ztfgroup.supervise.entity.Dept;
import com.ztfgroup.supervise.entity.Task;
import lombok.Data;

import java.util.List;

/**
 * @author: hejr
 * @desc:
 * @date: 2019/3/12 13:40
 */
@Data
public class CategoryVO extends Category {

    private List<Task> taskList;
    private List<Dept> deptList;
    private List<FeedbackVO> feedbackVOList;

}
