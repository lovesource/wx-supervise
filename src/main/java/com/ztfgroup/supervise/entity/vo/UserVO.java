package com.ztfgroup.supervise.entity.vo;

import com.ztfgroup.supervise.entity.User;
import com.ztfgroup.supervise.entity.UserTask;
import lombok.Data;

@Data
public class UserVO extends User {

    private UserTask userTask;
}
