package com.ztfgroup.supervise.entity.vo;

import com.ztfgroup.supervise.entity.Category;
import com.ztfgroup.supervise.entity.Task;
import com.ztfgroup.supervise.entity.User;
import lombok.Data;

import java.util.List;

@Data
public class TaskFeedbackVO extends Task {

    private Category category;
    private List<User> userList;
    private List<FeedbackVO> feedbackVoList;
}
