package com.ztfgroup.supervise.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 指示用户记录表
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
@TableName("TB_INSTRUCT_USER_STATUS")
@Data
@ToString
public class InstructUserStatus extends Model<InstructUserStatus> {

    private static final long serialVersionUID = 1L;

    /**
     * 指示ID
     */
    @TableId("INSTRUCT_ID")
    private String instructId;

    /**
     * 人员ID
     */
    @TableField("USER_ID")
    private String userId;

    /**
     * 已读时间
     */
    @TableField("READ_TIME")
    private String readTime;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
