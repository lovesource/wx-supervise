package com.ztfgroup.supervise.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 指示表
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
@TableName("TB_INSTRUCT")
@Data
@ToString
public class Instruct extends Model<Instruct> {

    private static final long serialVersionUID = 1L;

    /**
     * 指示ID
     */
    @TableId("INSTRUCT_ID")
    private String instructId;

    /**
     * 指示详情
     */
    @TableField("INSTRUCT_CONTENT")
    private String instructContent;

    /**
     * 部门ID
     */
    @TableField("DEPT_IDS")
    private String deptIds;

    /**
     * 项目(板块)ID
     */
    @TableField("CATEGORY_IDS")
    private String categoryIds;

    /**
     * 人员ID
     */
    @TableField("USER_IDS")
    private String userIds;

    /**
     * 录入时间
     */
    @TableField("TIME")
    private String time;

    @Override
    protected Serializable pkVal() {
        return this.instructId;
    }

}
