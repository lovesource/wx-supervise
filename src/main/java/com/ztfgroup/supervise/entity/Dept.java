package com.ztfgroup.supervise.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 部门表
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
@TableName("TB_DEPT")
@Data
@ToString
public class Dept extends Model<Dept> {

    private static final long serialVersionUID = 1L;

    /**
     * 部门ID
     */
    @TableId("DEPT_ID")
    private String deptId;

    /**
     * 部门名称
     */
    @TableField("DEPT_NAME")
    private String deptName;

    /**
     * 排序字段
     */
    @TableField("ORDER_NUM")
    private String orderNum;

    @Override
    protected Serializable pkVal() {
        return this.deptId;
    }

}
