package com.ztfgroup.supervise.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 任务清单表
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
@TableName("TB_TASK")
@Data
@ToString
public class Task extends Model<Task> {

    private static final long serialVersionUID = 1L;

    /**
     * 事项ID
     */
    @TableId("TASK_ID")
    private String taskId;

    /**
     * 事项名称
     */
    @TableField("TASK_NAME")
    private String taskName;

    /**
     * 事项详情
     */
    @TableField("TASK_CONTENT")
    private String taskContent;

    /**
     * 部门ID
     */
    @TableField("DEPT_ID")
    private String deptId;

    /**
     * 项目(板块)ID
     */
    @TableField("CATEGORY_ID")
    private String categoryId;

    /**
     * 事项反馈频次
     */
    @TableField("FREQUENCY")
    private String frequency;

    /**
     * 事项录入时间
     */
    @TableField("START_TIME")
    private String startTime;

    /**
     * 事项截止时间
     */
    @TableField("CLOSE_TIME")
    private String closeTime;

    /**
     * 完成时间
     */
    @TableField("FINISH_TIME")
    private String finishTime;

    /**
     * 状态
     */
    @TableField("STATUS")
    private String status;

    @Override
    protected Serializable pkVal() {
        return this.taskId;
    }

}
