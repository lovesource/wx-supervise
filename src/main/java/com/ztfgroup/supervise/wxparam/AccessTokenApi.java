package com.ztfgroup.supervise.wxparam;

public interface AccessTokenApi {

    /**
     * 获取accessToken缓存
     * @param corpId
     * @param corpSecret
     * @return
     */
    public String getAccessToken(String corpId,String corpSecret) throws Exception;
}
