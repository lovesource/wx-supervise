package com.ztfgroup.supervise.common;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @desc: 基础控制器
 * @author:hejr
 * @date:2019/2/27
 */
@Slf4j
public class SuperController {

    @Autowired
    protected HttpServletRequest request;


    /**
     * 获取session用户ID
     *
     * @return
     */
    protected String getSessionUserInfo() {
        HttpSession session = request.getSession();
//		String selfUserId = String.valueOf(session.getAttribute(Constants.USER_ID));
        String selfUserId = "ZSD999";
        log.info("从session中获取用户ID：" + selfUserId);
        return selfUserId;
    }


    /**
     * 重定向至地址 url
     *
     * @param url 请求地址
     * @return
     */
    protected String redirectTo(String url) {
        StringBuffer rto = new StringBuffer("redirect:");
        rto.append(url);
        return rto.toString();
    }


    /**
     * 返回 JSON 格式对象
     *
     * @param object 转换对象
     * @return
     */
    protected String toJson(Object object) {
        return JSON.toJSONString(object, SerializerFeature.BrowserCompatible);
    }


    /**
     * 返回 JSON 格式对象
     *
     * @param object 转换对象
     * @param format 序列化特点
     * @return
     */
    protected String toJson(Object object, String format) {
        if (format == null) {
            return toJson(object);
        }
        return JSON.toJSONStringWithDateFormat(object, format, SerializerFeature.WriteDateUseDateFormat);
    }

}
