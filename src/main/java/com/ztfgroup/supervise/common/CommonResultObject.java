package com.ztfgroup.supervise.common;


/**
* @desc: 公共返回实体类
* @author:hejr
* @date:2019/2/25
*/
public class CommonResultObject {

    /**
     * 可为数组或字符串等，可记录错误信息或正常返回的相关信息
     */
    private Object message;

    /**
     * 返回码  200代表正常  -1代表失败
     */
    private int code;

    private Object result;


    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
