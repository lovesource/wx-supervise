package com.ztfgroup.supervise.controller;

import com.hszsd.weixin.api.SmsAPI;
import com.hszsd.weixin.in.TextcardIn;
import com.hszsd.weixin.in.TextcardMessageIn;
import com.ztfgroup.supervise.intercept.RequiredWxOauth2;
import com.ztfgroup.supervise.wxparam.AccessTokenApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;
import java.util.List;

@Controller
public class WeixinTestController {

    @Value("${supervise.agentid}")
    private Integer agentid;

    @Value("${supervise.corpid}")
    private String corpid;

    @Value("${supervise.corpsecret}")
    private String corpsecret;

    @Autowired
    private AccessTokenApi accessTokenApi;

    @RequiredWxOauth2
    @RequestMapping(value = "loginTest")
    public String loginTest(){

        return "loginTest";
    }


    @RequestMapping(value = "smsTest")
    public String smsTest(String wxUserId) throws Exception {
        String description="<div class=\"gray\">2019年2月26日</div> <div class=\"normal\">" +
                "恭喜你成功调试推送消息</div><div class=\"highlight\">" +
                "感谢您的支持</div>";
        String accessToken= accessTokenApi.getAccessToken(corpid,corpsecret);
        TextcardMessageIn textcardMessageIn=new TextcardMessageIn();
        textcardMessageIn.setTouser(wxUserId);
        textcardMessageIn.setAgentid(agentid);
        textcardMessageIn.setMsgtype("textcard");
        TextcardIn textcardIn=new TextcardIn();
        textcardIn.setBtntxt("看一看");
        textcardIn.setTitle("测试标题");
        textcardIn.setDescription(description);
        textcardIn.setUrl("http://www.baidu.com");
        textcardMessageIn.setTextcard(textcardIn);
        SmsAPI.sendMessage(accessToken,textcardMessageIn);
        return "asda";
    }

    class User{
        String name;
        String pwd;
        String age;

        public User(String name, String pwd, String age) {
            this.name = name;
            this.pwd = pwd;
            this.age = age;
        }
    }

    public static void main(String[] args) {
        String[] nameArr = "hejinrong, liujing".split(",");
        String[] pwdArr = "123456, 22222".split(",");
        String[] ageArr = "23,25".split(",");

        List<String> nameList = Arrays.asList(nameArr);
        List<String> pwdList = Arrays.asList(pwdArr);
        List<String> ageList = Arrays.asList(ageArr);



    }
}
