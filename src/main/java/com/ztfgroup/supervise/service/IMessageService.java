package com.ztfgroup.supervise.service;

import com.ztfgroup.supervise.entity.Message;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hejr
 * @since 2019-03-07
 */
public interface IMessageService extends IService<Message> {

}
