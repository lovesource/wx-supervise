package com.ztfgroup.supervise.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ztfgroup.supervise.entity.Feedback;
import com.ztfgroup.supervise.entity.vo.FeedbackVO;

import java.util.List;

/**
 * <p>
 * 任务跟踪表 服务类
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
public interface IFeedbackService extends IService<Feedback> {

    /**
     * 按任务TASK_ID查询反馈列表
     *
     * @param taskId 任务TASK_ID
     * @return 反馈列表
     */
    List<FeedbackVO> findFeedbackByTaskId(String taskId);


    /**
     * 新增反馈信息
     *
     * @param roleId
     * @param userId
     * @param taskId
     * @param taskStatus
     * @param feedbackContent
     * @return
     */
    Integer addFeeback(String roleId, String userId, String taskId, String taskStatus, String feedbackContent);

}
