package com.ztfgroup.supervise.service;

import com.ztfgroup.supervise.entity.CategoryDept;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 项目板块部门表 服务类
 * </p>
 *
 * @author hejr
 * @since 2019-03-06
 */
public interface ICategoryDeptService extends IService<CategoryDept> {

}
