package com.ztfgroup.supervise.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ztfgroup.supervise.entity.InstructFeedback;
import com.ztfgroup.supervise.entity.vo.InstructFeedbackVO;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hejr
 * @since 2019-03-14
 */
public interface IInstructFeedbackService extends IService<InstructFeedback> {


    /**
     * 新增指示反馈
     *
     * @param userId
     * @param instructId
     * @param roleId
     * @param feedbackContent
     * @return
     */
    Integer addInstructFeeback(String userId, String instructId, Integer roleId, String feedbackContent);


    /**
     * 查询指示反馈
     *
     * @param instructId
     * @return
     */
    List<InstructFeedbackVO> findInstructFeedBack(String instructId);
}
