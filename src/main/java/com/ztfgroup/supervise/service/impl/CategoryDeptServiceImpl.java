package com.ztfgroup.supervise.service.impl;

import com.ztfgroup.supervise.entity.CategoryDept;
import com.ztfgroup.supervise.mapper.CategoryDeptMapper;
import com.ztfgroup.supervise.service.ICategoryDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 项目板块部门表 服务实现类
 * </p>
 *
 * @author hejr
 * @since 2019-03-06
 */
@Service
public class CategoryDeptServiceImpl extends ServiceImpl<CategoryDeptMapper, CategoryDept> implements ICategoryDeptService {

}
