package com.ztfgroup.supervise.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ztfgroup.supervise.entity.UserTask;
import com.ztfgroup.supervise.entity.vo.RoleTaskVO;
import com.ztfgroup.supervise.entity.vo.UserTaskVO;
import com.ztfgroup.supervise.mapper.UserTaskMapper;
import com.ztfgroup.supervise.service.IUserTaskService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 人员任务表 服务实现类
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
@Service
public class UserTaskServiceImpl extends ServiceImpl<UserTaskMapper, UserTask> implements IUserTaskService {


    @Override
    public RoleTaskVO findRoleTaskInfo(String userId, String taskId) {
        return baseMapper.findRoleTaskInfo(userId, taskId);
    }

    @Override
    public List<UserTaskVO> findUserInfoByTaskId(String userId, String taskId) {
        return baseMapper.findUserInfoByTaskId(userId, taskId);
    }

}
