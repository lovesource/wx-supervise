package com.ztfgroup.supervise.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ztfgroup.supervise.entity.Category;
import com.ztfgroup.supervise.entity.vo.CategoryVO;
import com.ztfgroup.supervise.mapper.CategoryMapper;
import com.ztfgroup.supervise.service.ICategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 项目板块表 服务实现类
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements ICategoryService {

    @Override
    public List<Category> findAllTaskCategory(String userId) {
        return baseMapper.findTaskCategory(userId);
    }

    @Override
    public List<Category> findAllCategory() {
        return baseMapper.findAllCategory();
    }

    @Override
    public List<CategoryVO> findTaskAndNewestFeedback(String selfUserId, String deptId, String otherUserId) {
        return baseMapper.findTaskAndNewestFeedback(selfUserId, deptId, otherUserId);
    }

    @Override
    public List<CategoryVO> findCategoryDeptList(String selfUserId, String categoryId) {
        return baseMapper.findCategoryDeptList(selfUserId, categoryId);
    }

    @Override
    public List<CategoryVO> findTaskTrackList(String selfUserId) {
        return baseMapper.findTaskTrackList(selfUserId);
    }
}
