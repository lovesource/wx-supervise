package com.ztfgroup.supervise.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hszsd.weixin.api.SmsAPI;
import com.hszsd.weixin.in.TextcardIn;
import com.hszsd.weixin.in.TextcardMessageIn;
import com.hszsd.weixin.out.GetUserInfoOut;
import com.ztfgroup.supervise.entity.InstructFeedback;
import com.ztfgroup.supervise.entity.Message;
import com.ztfgroup.supervise.entity.User;
import com.ztfgroup.supervise.entity.vo.InstructFeedbackVO;
import com.ztfgroup.supervise.entity.vo.InstructVO;
import com.ztfgroup.supervise.mapper.InstructFeedbackMapper;
import com.ztfgroup.supervise.mapper.MessageMapper;
import com.ztfgroup.supervise.service.IInstructFeedbackService;
import com.ztfgroup.supervise.service.IInstructService;
import com.ztfgroup.supervise.service.IUserService;
import com.ztfgroup.supervise.util.Constants;
import com.ztfgroup.supervise.wxparam.AccessTokenApi;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author hejr
 * @since 2019-03-14
 */
@Service
@Slf4j
public class InstructFeedbackServiceImpl extends ServiceImpl<InstructFeedbackMapper, InstructFeedback> implements IInstructFeedbackService {

    @Autowired
    private IUserService userService;

    @Value("${supervise.agentId}")
    private Integer agentId;

    @Value("${supervise.corpId}")
    private String corpId;

    @Value("${supervise.corpSecret}")
    private String corpSecret;


    @Value("${supervise.domainHost}")
    private String domainHost;

    @Autowired
    private AccessTokenApi accessTokenApi;

    @Autowired
    private IInstructService instructService;

    @Autowired
    private MessageMapper messageMapper;

    @Override
    public Integer addInstructFeeback(String userId, String instructId, Integer roleId, String feedbackContent) {

        Integer result = -1;
        InstructFeedback feedback = new InstructFeedback();
        feedback.setFeedbackId(Constants.INSTRUCT_FEEDBACK_ID_PREX);
        feedback.setUserId(userId);
        feedback.setRoleId(roleId);
        feedback.setInstructId(instructId);
        feedback.setFeedbackContent(StringEscapeUtils.escapeHtml4(feedbackContent));
        feedback.setFeedbackTime(Constants.DATE_FORMAT_SPECIAL_DAY.format(new Date()));
        try {
            result = baseMapper.addInstructFeeback(feedback);
            //异步推送消息
            if (result > 0) {
                String wxUserIds = "";
                User user = userService.selectById(userId);
                //董事长角色，推送给与当前指示相关的其他负责人
                if (user != null && user.getLv().equals(Constants.LEADER_ROLE_ID)) {
                    //获取其他人员的微信号
                    InstructVO instructVO = instructService.findInstructAndUserNameById(instructId);
                    List<User> userList = instructVO.getUserList();
                    if (userList != null && !userList.isEmpty()) {
                        StringBuffer sb = new StringBuffer("");
                        userList.stream().forEach(l -> {
                            sb.append(l.getWxUserId()).append("|");
                        });
                        wxUserIds = sb.toString().substring(0, sb.toString().length() - 1);
                    }
                } else {
                    //获取董事长微信号
                    User leadUser = userService.selectOne(new QueryWrapper<User>().eq("LV", Constants.LEADER_ROLE_ID));
                    if (leadUser != null && leadUser.getLv() != null) {
                        wxUserIds = leadUser.getWxUserId();
                    }
                }
                Thread t = new Thread(new InstructFeedbackClusterThread(instructId, user, wxUserIds));
                t.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<InstructFeedbackVO> findInstructFeedBack(String instructId) {
        return baseMapper.findInstructFeedBack(instructId);
    }

    /**
     * 反馈信息微信推送内部类
     */
    class InstructFeedbackClusterThread implements Runnable {

        String instructId;
        User user;
        String wxUserIds;

        public InstructFeedbackClusterThread() {
        }

        public InstructFeedbackClusterThread(String instructId, User user, String wxUserIds) {
            this.instructId = instructId;
            this.user = user;
            this.wxUserIds = wxUserIds;
        }

        @Override
        public void run() {
            log.info("begin instructfeedback push....");
            try {
                sendInstructFeedback(instructId, user, wxUserIds);
            } catch (Exception e) {
                log.error("instructfeedback push is abnormal...");
            }
            log.info("instructfeedback push is over...");
        }
    }

    private void sendInstructFeedback(String instructId, User user, String wxUserIds) {
        try {
            //当前时间
            String dateTime = Constants.DATE_FORMAT_SEND_TIME.format(new Date());
            //推送内容
            String description = "";
            //董事长
            if (user.getLv().equals(Constants.LEADER_ROLE_ID)) {
                description = Constants.INSTRUCT_FEEDBACK_MSG_TEMPLATE.replace("${userName}", "董事长")
                        .replace("${roleId}", "指示")
                        .replace("${dateTime}", dateTime);
            } else {
                description = Constants.INSTRUCT_FEEDBACK_MSG_TEMPLATE.replace("${userName}", user.getUserName())
                        .replace("${roleId}", "反馈")
                        .replace("${dateTime}", dateTime);
            }
            String accessToken = accessTokenApi.getAccessToken(corpId, corpSecret);
            TextcardMessageIn textcardMessageIn = new TextcardMessageIn();
            //设置推送用户ID
            textcardMessageIn.setTouser(wxUserIds);
            textcardMessageIn.setAgentid(agentId);
            textcardMessageIn.setMsgtype("textcard");
            TextcardIn textcardIn = new TextcardIn();
            textcardIn.setBtntxt("详情");
            if (user.getLv().equals(Constants.LEADER_ROLE_ID)) {
                textcardIn.setTitle("董事长指示");
            } else {
                textcardIn.setTitle(user.getUserName() + "反馈");
            }
            textcardIn.setDescription(description);
            textcardIn.setUrl("http://" + domainHost + "/instructdetail?instructId=" + instructId);
            textcardMessageIn.setTextcard(textcardIn);
            GetUserInfoOut infoOut = SmsAPI.sendMessage(accessToken, textcardMessageIn);
            //往消息表中新增数据
            Message message = new Message();
            message.setId(Constants.MESSAGE_ID_PREX);
            message.setSendTime(Constants.DATE_FORMAT_SPECIAL_DAY.format(new Date()));
            message.setSender(user.getUserId());
            message.setReceiver(wxUserIds);
            message.setContent(description);
            message.setType("D");
            message.setStatus(String.valueOf(infoOut.getErrcode()));
            Integer ret = messageMapper.addMessage(message);
            log.info("指示消息入库" + (ret > 0 ? "成功" : "失败"));
        } catch (Exception e) {
            log.error("指示消息推送失败，userId：{}, wxUserIds：{}", user.getUserId(), wxUserIds);
            e.printStackTrace();
        }
    }
}
