package com.ztfgroup.supervise.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ztfgroup.supervise.entity.Dept;
import com.ztfgroup.supervise.mapper.DeptMapper;
import com.ztfgroup.supervise.service.IDeptService;
import com.ztfgroup.supervise.util.StringUtil;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 部门表 服务实现类
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
@Service
public class DeptServiceImpl extends ServiceImpl<DeptMapper, Dept> implements IDeptService {

    @Override
    public List<Dept> findAllTaskDept(String userId) {
        return baseMapper.findTaskDept(userId, null);
    }

    @Override
    public List<Dept> findTaskDept(String userId, String categoryId) {
        return baseMapper.findTaskDept(userId, categoryId);
    }

    @Override
    public List<Dept> findAllDept(String categoryIds) {
        return baseMapper.findAllDept(StringUtil.convertQueryIds(categoryIds));
    }
}
