package com.ztfgroup.supervise.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ztfgroup.supervise.entity.InstructUserStatus;
import com.ztfgroup.supervise.mapper.InstructUserStatusMapper;
import com.ztfgroup.supervise.service.IInstructUserStatusService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ztfgroup.supervise.util.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 * 指示用户记录表 服务实现类
 * </p>
 *
 * @author hejr
 * @since 2019-03-06
 */
@Service
@Slf4j
public class InstructUserStatusServiceImpl extends ServiceImpl<InstructUserStatusMapper, InstructUserStatus> implements IInstructUserStatusService {


    @Override
    public void dealWithInstructInfo(String selfUserId, String instructId) {
        log.info("处理当前用户是否已读该指示，参数：userId：{}，instructId：{}", selfUserId, instructId);
        Integer count = baseMapper.selectCount(new QueryWrapper<InstructUserStatus>().eq("USER_ID", selfUserId).eq("INSTRUCT_ID", instructId));
        //如果为0，表示之前未读过，新增已读记录
        if (count == 0) {
            InstructUserStatus instructUserStatus = new InstructUserStatus();
            instructUserStatus.setUserId(selfUserId);
            instructUserStatus.setInstructId(instructId);
            instructUserStatus.setReadTime(Constants.DATE_FORMAT_SPECIAL_DAY.format(new Date()));
            Integer ret = baseMapper.addInstructUserStatus(instructUserStatus);
            log.info("新增已读记录 " + (ret > 0 ? "成功" : "失败") + ", userId: {}，instructId: {}", selfUserId, instructId);
        }
    }
}
