package com.ztfgroup.supervise.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ztfgroup.supervise.entity.Category;
import com.ztfgroup.supervise.entity.vo.CategoryVO;

import java.util.List;

/**
 * <p>
 * 项目板块表 服务类
 * </p>
 *
 * @author hejr
 * @since 2019-02-25
 */
public interface ICategoryService extends IService<Category> {

    /**
     * 按用户ID获取全部板块
     *
     * @param userId
     * @return
     */
    List<Category> findAllTaskCategory(String userId);

    /**
     * 查询所有板块
     *
     * @return
     */
    List<Category> findAllCategory();

    /**
     * 查询任务信息及最新反馈
     *
     * @param selfUserId
     * @param deptId
     * @param otherUserId
     * @return
     */
    List<CategoryVO> findTaskAndNewestFeedback(String selfUserId, String deptId, String otherUserId);


    /**
     * 查询板块及公司信息
     * @param selfUserId
     * @param categoryId
     * @return
     */
    List<CategoryVO> findCategoryDeptList(String selfUserId, String categoryId);


    /**
     * 查询任务追踪列表
     * @param selfUserId
     * @return
     */
    List<CategoryVO> findTaskTrackList(String selfUserId);
}
