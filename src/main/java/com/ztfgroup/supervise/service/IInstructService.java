package com.ztfgroup.supervise.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ztfgroup.supervise.entity.Instruct;
import com.ztfgroup.supervise.entity.vo.InstructVO;

import java.util.List;

/**
 * <p>
 * 指示表 服务类
 * </p>
 *
 * @author hejr
 * @since 2019-03-06
 */
public interface IInstructService extends IService<Instruct> {


    /**
     * 获取历史指示信息
     * @param categoryId
     * @param deptId
     * @param userId
     * @param dateType 日期类型 1：最近一周 2：最近一月 3：一个月以前
     * @param currentUserRoleId 当前用户角色ID
     * @param selfUserId
     * @return
     */
    List<InstructVO> findAllInstruct(String categoryId, String deptId, String userId, String dateType, Integer currentUserRoleId, String selfUserId);


    /**
     * 新增指示
     *
     * @param categoryIds 板块ID
     * @param deptIds     公司ID
     * @param userIds     用户ID
     * @param content     指示内容
     * @param selfUserId  自身ID
     * @return
     */
    Integer addInstruct(String categoryIds, String deptIds, String userIds, String content, String selfUserId);


    /**
     * 根据用户ID和指示ID判断是否可以查看指示
     *
     * @param selfUserId
     * @param instructId
     * @return
     */
    Boolean judgeIsScanInstruct(String selfUserId, String instructId);


    /**
     * 根据指示ID获取指示信息及对应指示的人员
     *
     * @param instructId
     * @return
     */
    InstructVO findInstructAndUserNameById(String instructId);
}
