package com.ztfgroup.supervise.intercept;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Description:微信登录拦截器
 * @Author 杨文建
 * @Create 2017年04月28日 9:37
 * @Copyright 个人版权所有
 * @Company 贵州合石科技有限公司Copyright (c) 2017
 * @Version V1.0
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RequiredWxOauth2 {
    public String state() default "";
}
