package com.ztfgroup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
* @desc: 启动类
* @author:hejr
* @date:2019/2/26
*/
@EnableCaching
@SpringBootApplication
public class WxSuperviseApplication {

	public static void main(String[] args) {
		SpringApplication.run(WxSuperviseApplication.class, args);
	}


	@Bean
	public InternalResourceViewResolver setupViewResolver(){
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/jsp/");
		resolver.setSuffix(".jsp");
		return resolver;
	}
}
