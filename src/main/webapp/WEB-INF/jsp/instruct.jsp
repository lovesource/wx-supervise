<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0"/>
    <title>指示页面</title>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css"/>
    <script type="text/javascript" src="/jquery/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/shuiyin.js"></script>
</head>
<body>
<style>
    * {
        padding: 0;
        margin: 0;
        -moz-user-select: none; /*火狐*/
        -webkit-user-select: none; /*webkit浏览器*/
        -ms-user-select: none; /*IE10*/
        -khtml-user-select: none; /*早期浏览器*/
        user-select: none;
        overflow-x: hidden;
    }

    .textArea{
        margin: 15px 1px;
        -moz-user-select:text !important;/*火狐*/
        -webkit-user-select:text !important;/*webkit浏览器*/
        -ms-user-select:text !important;/*IE10*/
        -khtml-user-select:text !important;/*早期浏览器*/
    }

    .content {
        width: 100%;
        min-height: 100vh;
        overflow: hidden;
        background-color: #f0f2f9;
        padding: 10px;
    }

    .nav-tabs li {
        width: 50%;
        text-align: center;
        color: #333333;
    }

    .plate {
        width: 100%;
        padding: 10px;
        min-height: 50px;
        background-color: #ffffff;
    }

    .timeItem {
        float: left;
        min-width: 55px;
        height: 30px;
        font-size: 14px;
        line-height: 30px;
        margin-left: 12px;
        text-align: center;
        color: #07349a;
        cursor: pointer;
    }

    .timeItem.active {
        border-bottom: 1px solid #ee3148;
        color: #ee3148;
    }

    .timeItem.selected {
        color: #747474;
        cursor: pointer;
    }

    .companyItem, .userItem, .plateItem, .sidePlateItem, .sideCompanyItem, .sideUserItem {
        float: left;
        min-width: 50px;
        height: 26px;
        font-size: 12px;
        line-height: 26px;
        margin-left: 6px;
        margin-top: 6px;
        text-align: center;
        padding: 0px 5px;
        background-color: #f2f2f2;
        border-radius: 8px;
    }

    .companyItem.active, .userItem.active, .plateItem.active {
        background-color: #ffffff;
        border: 1px solid #07349a;
        color: #07349a;
    }
    .sidePlateItem.active, .sideCompanyItem.active, .sideUserItem.active {
        background-color: #ffffff;
        border: 1px solid #07349a;
        color: #07349a;
    }

    .indicateList .indicateItem {
        margin-top: 5px;
        background-color: #ffffff;
        min-height: 100px;
        padding: 10px;
    }

    .indicateList .indicateItem p {
        padding-top: 5px;
        font-size: 12px;
        color: #666666;
        word-break: break-all;
    }

    .modal-dialog {
        margin: 0px;
    }

    /*Right*/
    .modal.right.fade .modal-dialog {
        right: -320px;
        -webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
        -moz-transition: opacity 0.3s linear, right 0.3s ease-out;
        -o-transition: opacity 0.3s linear, right 0.3s ease-out;
        transition: opacity 0.3s linear, right 0.3s ease-out;
    }

    .modal.right.fade.in .modal-dialog {
        right: 0;
    }

    .form-group {
        /*border-bottom: 1px solid #333333;*/
    }

</style>
<div class="content" id="parent_content_area">
    <div id="content_area">
        <ul class="nav nav-tabs" role="tablist">
            <li id="new_instruct_li_id" role="presentation"><a href="#new" aria-controls="home" role="tab"
                                                      data-toggle="tab" onclick="init()">新指示</a></li>
            <li id="history_instruct_li_id" role="presentation"><a href="#history" aria-controls="profile" role="tab" data-toggle="tab" onclick="queryHistroyInstruct()">历史指示</a>
            </li>
        </ul>
        <div class="tab-content" id="tab_content_id">
            <div role="tabpanel" class="tab-pane" id="new" style=" background-color: #ffffff;padding: 8px">
                <form class="form-horizontal">
                    <div class="form-group form-group-sm">
                         <label class="col-sm-2 control-label">板块：</label>
                         <div class="col-sm-10">
                             <div id="all_plate" class="plateItem active" onclick="switchPlate(this, 'all_plate', '')">全部</div>
                         </div>
                     </div>
                    <div class="form-group form-group-sm">
                        <label class="col-sm-2 control-label">公司：</label>
                        <div class="col-sm-10">
                            <div id="all_company" class="companyItem active" onclick="switchCompany(this, 'all_company', '')">全部</div>
                        </div>
                    </div>
                    <div class="form-group form-group-sm">
                        <label class="col-sm-2 control-label">人员：</label>
                        <div class="col-sm-10">
                            <div id="all_user" class="userItem" onclick="switchUser(this, 'all_user', '')">全部</div>
                            <%--<div class="userItem" name="user" id="11111" onclick="switchUser(this, 'user', '111')">金融</div>--%>
                        </div>
                    </div>
                    <div class="form-group form-group-sm">
                        <label class="col-sm-2 control-label">指示内容：</label>
                        <div class="col-sm-10">
                            <textarea id="instruct_content_id" class="form-control textArea" rows="6" maxlength="1000"
                                      placeholder="请输入指示内容（仅限1000字内）"></textarea>
                        </div>
                    </div>
                    <div style="text-align: center;margin: 10px">
                        <button type="button" class="btn btn-primary" style="width: 80%" onclick="commit()">提交</button>
                    </div>
                </form>
            </div>
            <div role="tabpanel" class="tab-pane" id="history">
                <div class="plate">
                    <div class="timeItem active" onclick="switchDate(this, 1)">最近一周</div>
                    <div class="timeItem" onclick="switchDate(this, 2)">最近一个月</div>
                    <div class="timeItem" onclick="switchDate(this, 3)">一个月以前</div>
                    <div id="filter_id" style="float: right" class='timeItem' data-toggle='modal' data-target='#myModal'>筛选 <span
                            class='glyphicon glyphicon-filter' aria-hidden='true'></span></div>
                    <div style="clear: both"></div>
                </div>
                <div class="modal right fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document" style="margin-left: 100px;">
                        <div class="modal-content" style="min-height: 100vh">
                            <div class="modal-header">
                                <p>板块：</p>
                                <div id="categoryListId"></div>
                                <%--<div class="plateItem" onclick="switchPrams(this,'plate')">金融</div>--%>
                                <div style="clear: both"></div>
                            </div>
                            <div class="modal-body">
                                <p>公司：</p>
                                <div id="deptListId">

                                </div>
                                <%--<div class="companyItem" onclick="switchPrams(this,'company')">未来方舟123</div>--%>
                                <div style="clear: both"></div>
                            </div>
                            <div class="modal-body">
                                <p>人员：</p>
                                <div id="userListId"></div>
                                <%--<div class="userItem" onclick="switchPrams(this,'user')">张小三</div>--%>
                                <div style="clear: both"></div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary"
                                        style="width: 90px " onclick="reset()">重置
                                </button>
                                <button type="button" class="btn btn-primary"
                                        style="width: 90px " data-dismiss="modal">确认
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="indicateList" id="history_instruct_id">
                    <%--<div class="indicateItem">--%>
                        <%--<span>20190306</span><br>--%>
                        <%--<p>张三(已读)、李四(未读)</p>--%>
                        <%--<p style="margin-top: 5px">杯子在多数时候，盛装的仅仅是半杯水，遇见的那个人依然似乎无法填补内心的空洞，时间一点一点地过去，--%>
                            <%--水也一点一点地蒸发掉，你开始觉得他越来越无法满足你的全部幻想全部期待，--%>
                            <%--对望的时候，彼此的眼里充满了空洞犹疑。杯子里面的水终于全部消失。</p>--%>
                    <%--</div>--%>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="category_id" value=""/>
    <input type="hidden" id="dept_id" value=""/>
    <input type="hidden" id="other_user_id" value=""/>
    <input type="hidden" id="date_type_id" value=""/>
    <input type="hidden" id="current_user_role_id" value="${currentUserRole}"/>
</div>

<script>

    var categoryArr = [];
    var deptArr = [];
    var userArr = [];

    $(function () {
        //如果角色ID为4，即为董事长，加载所有
        var currentUserRoleId = $("#current_user_role_id").val();
        //如果该用户都不在TB_USER中，则啥都看不到
        if(currentUserRoleId != '') {
            if(currentUserRoleId == 1) {
                $("#new_instruct_li_id").addClass("active");
                $("#new").addClass("active");
                //加载板块信息
                loadAllCategoryList(0);
                //加载公司信息
                loadAllDeptList(0);
                //加载人员信息
                loadAllUserList(0);
            } else {
                $("#new_instruct_li_id").css("display", "none");
                $("#new").css("display", "none");
                $("#history_instruct_li_id").addClass("active");
                $("#history").addClass("active");
                $("#filter_id").css("display", "none");
                queryHistroyInstruct();
            }
        } else {
            $("#content_area").css("display", "none");
            $("#parent_content_area").html("<label color='red'>您没有权限，请联系管理员!</label>");
        }
    });

    function init() {
        categoryArr = [];
        deptArr = [];
        userArr = [];
        //加载板块信息
        loadAllCategoryList(0);
        //加载公司信息
        loadAllDeptList(0);
        //加载人员信息
        loadAllUserList(0);
    }


    function queryHistroyInstruct() {
        console.log("查询历史指示信息...")
        var params = getQueryParams();
        console.log(params)
        $.post("findAllInstruct", params, function (instructVOList) {
            if(instructVOList.length > 0) {
                $("#history_instruct_id").html(dealWithHistoryInstructList(instructVOList));
            } else {
                $("#history_instruct_id").html("<span color='gray'>暂无数据!!!</span>");
            }
        });
    }

    /**
     * 获取参数
     */
    function getQueryParams() {
        var categoryId = $("#category_id").val();
        var deptId = $("#dept_id").val();
        var otherUserId = $("#other_user_id").val();
        var dateType = $("#date_type_id").val();
        var currentUserRoleId = $("#current_user_role_id").val();
        var params = {"categoryId": categoryId, "deptId": deptId, "userId": otherUserId, "dateType": dateType, "currentUserRoleId": currentUserRoleId};
        return params;
    }


    function dealWithCategoryResultList(categoryList) {
        var divHtml = "";
        for(var i in categoryList) {
            divHtml += '<div class="sidePlateItem" onclick="switchPrams(this, \'plate\', \'' + categoryList[i].categoryId + '\')">' + categoryList[i].categoryName + '</div>';
        }
        return divHtml;
    }

    function dealWithDeptResultList(deptList) {
        var divHtml = "";
        for(var i in deptList) {
            divHtml += '<div class="sideCompanyItem" onclick="switchPrams(this, \'company\', \'' + deptList[i].deptId + '\')">' + deptList[i].deptName + '</div>';
        }
        return divHtml;
    }

    function dealWithUserResultList(userList) {
        var divHtml = "";
        for(var i in userList) {
            divHtml += '<div class="sideUserItem" onclick="switchPrams(this, \'user\', \'' + userList[i].userId + '\')">' + userList[i].userName + '</div>';
        }
        return divHtml;
    }



    function dealWithHistoryInstructList(instructVOList) {
        cleanWaterMark();
        var divHtml = '';
        for(var i in instructVOList) {
            divHtml += '<div class="indicateItem" onclick="goInstructDetailPage(\'' + instructVOList[i].instructId + '\')">';
            divHtml += '<span>' + instructVOList[i].time + '</span><br>';
            divHtml += '<p>';
            for(var j in instructVOList[i].userInstructVOList) {
                divHtml += '' + instructVOList[i].userInstructVOList[j].userName + '(' + (instructVOList[i].userInstructVOList[j].instructUserStatus.userId != null ? "已读" : "未读") + ')&nbsp;&nbsp;';
            }
            divHtml += '</p>';
            divHtml += '<p style="margin-top: 5px">' + instructVOList[i].instructContent + '</p>';
            divHtml += '</div>';
        }
        $.post("findUserName", null, function(user){
            var text = user.userName + " " + convertDate(new Date());
            setTimeout(watermarkCanvas({ watermark_txt: text }),1000);
        });
        return divHtml;
    }
    
    function goInstructDetailPage(instructId) {
        window.location.href = "goInstructDetailPage?instructId=" + instructId;
    }


    /**
     *  type 0：查询首页 1：查询侧边栏
     */
    function loadAllCategoryList(type) {
        console.log("加载板块信息.....")
        var params = (type == 0 ? null : getQueryParams());
        $.post("findAllCategory", params, function (categoryList) {
            if(type == 0) {
                $("#all_plate").addClass("active");
                $("[name='plate']").remove();
                //首页展示
                $("#all_plate").after(dealWithAllCategoryList(categoryList));
                //侧边栏展示
                $("#categoryListId").html(dealWithCategoryResultList(categoryList));
                //获取板块ID
                for(var i in categoryList) {
                    categoryArr.push(categoryList[i].categoryId)
                }
            } else {
                //侧边栏展示
                $("#categoryListId").html(dealWithCategoryResultList(categoryList));
            }
        });
    }

    function loadAllDeptList(type) {
        console.log("加载公司信息.....")
        var params = (type == 0 ? null : {"categoryIds": $("#category_id").val()});
        console.log(params)
        $.post("findAllDept", params, function (deptList) {
            if(type == 0) {
                $("#all_company").addClass("active");
                //首页展示
                $("[name='company']").remove();
                $("#all_company").after(dealWithAllDeptList(deptList));
                //侧边栏展示
                $("#deptListId").html(dealWithDeptResultList(deptList));
                //获取公司ID
                for(var i in deptList) {
                    deptArr.push(deptList[i].deptId)
                }
            } else {
                //侧边栏展示
                $("#deptListId").html(dealWithDeptResultList(deptList));
            }
        });
    }

    function loadAllUserList(type) {
        console.log("加载用户信息.....")
        var params = (type == 0 ? null : {"categoryIds": $("#category_id").val(), "deptIds": $("#dept_id").val()});
        console.log(params)
        $.post("findAllUser", params, function (userList) {
            if(type == 0) {
                $("[name='user']").remove();
                $("#all_user").after(dealWithAllUserList(userList));
                $("#userListId").html(dealWithUserResultList(userList));
                //获取用户ID
//                for(var i in userList) {
//                    userArr.push(userList[i].userId)
//                }
            } else {
                $("#userListId").html(dealWithUserResultList(userList));
            }
        });
    }

    function loadDeptListByCategoryIds() {
        var categoryIds = '';
        if(categoryArr.length > 0) {
            for(var i in categoryArr) {
                categoryIds += categoryArr[i] + ',';
            }
            categoryIds = categoryIds.substr(0, categoryIds.length -1);
        }
        var params = {"categoryIds": categoryIds};
        console.log(categoryIds)
        $.post("findAllDept", params, function (deptList) {
            //清除公司其他选中项
            $(".companyItem").removeClass("active")
            //把全部选中
            $("#all_company").addClass("active");
            $("[name='company']").remove();
            $("#all_company").after(dealWithAllDeptList(deptList));
            deptArr = [];
            //获取公司ID
            for(var i in deptList) {
                deptArr.push(deptList[i].deptId)
            }
            loadUserListByCategoryIdsDeptIds();
        });
    }

    function loadUserListByCategoryIdsDeptIds() {
        console.log("loadUserListByCategoryIdsDeptIds")
        var categoryIds = '';
        var deptIds = '';
        if(categoryArr.length > 0) {
            for(var i in categoryArr) {
                categoryIds += categoryArr[i] + ',';
            }
            categoryIds = categoryIds.substr(0, categoryIds.length -1);
        }
        if(deptArr.length > 0) {
            for(var i in deptArr) {
                deptIds += deptArr[i] + ',';
            }
            deptIds = deptIds.substr(0, deptIds.length -1);
        }
        var params = {"categoryIds": categoryIds, "deptIds": deptIds};
        console.log(params)
        $.post("findAllUser", params, function (userList) {
            $("[name='user']").remove();
            $("#all_user").after(dealWithAllUserList(userList));
            userArr = [];
            //获取用户ID
//            for(var i in userList) {
//                userArr.push(userList[i].userId)
//            }
        });
    }


    /**
     * 解析板块列表
     *
     */
    function dealWithAllCategoryList(categoryList) {
        var categoryListLenth = (categoryList != null ? categoryList.length : 0);
        var html = '';
        for (var i in categoryList) {
            html += '<div class="plateItem" name="plate" id="' + categoryList[i].categoryId + '" onclick="switchPlate(this, \'plate\', \'' + categoryList[i].categoryId + '\')"> ' + categoryList[i].categoryName + ' </div>';
        }
        html += '<input type="hidden" id="category_list_length_id" value="' + categoryListLenth +'"/>';
        return html;
    }

    /**
     * 解析部门列表
     *
     */
    function dealWithAllDeptList(deptList) {
        var deptListLenth = (deptList != null ? deptList.length : 0);
        var html = '';
        for (var i in deptList) {
            html += '<div class="companyItem" name="company" id="' + deptList[i].deptId + '" onclick="switchCompany(this, \'company\', \'' + deptList[i].deptId + '\')"> ' + deptList[i].deptName + ' </div>';
        }
        html += '<input type="hidden" id="dept_list_length_id" value="' + deptListLenth +'"/>';
        return html;
    }

    /**
     * 解析用户列表
     *
     */
    function dealWithAllUserList(userList) {
        var userListLenth = (userList != null ? userList.length : 0);
        var html = '';
        for (var i in userList) {
            html += '<div class="userItem" name="user" id="\' + userList[i].userId + \'" onclick="switchUser(this, \'user\', \'' + userList[i].userId + '\')">' + userList[i].userName + '</div>';
        }
        html += '<input type="hidden" id="user_list_length_id" value="' + userListLenth +'"/>';
        return html;
    }


    function commit() {
        var instructContent = $("#instruct_content_id").val().trim();

        if(userArr.length == 0) {
            alert("请选择人员");
            return false;
        }
        if(instructContent == "" || instructContent == undefined) {
            alert('请输入指示内容');
            $("#instruct_content_id").focus();
            return false;
        } $.post("addInstruct", dealWithNewInstruct(instructContent), function (ret) {
            if(ret > 0) {
                $("#instruct_content_id").val("");
            }
        });
        var params = dealWithNewInstruct(instructContent);
        console.log(params)

        alert("指示已提交");
    }


    /**
     * 获取选中的板块ID，公司ID，用户ID及指示内容，传入后台
     */
    function dealWithNewInstruct(instructContent) {
        var categoryIds = '';
        var deptIds = '';
        var userIds = '';
        if(categoryArr.length > 0) {
            for(var i in categoryArr) {
                categoryIds += categoryArr[i] + ',';
            }
            categoryIds = categoryIds.substr(0, categoryIds.length -1);
        }
        if(deptArr.length > 0) {
            for(var i in deptArr) {
                deptIds += deptArr[i] + ',';
            }
            deptIds = deptIds.substr(0, deptIds.length -1);
        }
        if(userArr.length > 0) {
            for(var i in userArr) {
                userIds += userArr[i] + ',';
            }
            userIds = userIds.substr(0, userIds.length -1);
        }
        var params = {"categoryIds": categoryIds, "deptIds": deptIds, "userIds": userIds, "content": instructContent};
        console.log(params)
        return params;
    }

    /**
     * 重置清空
     */
    function reset() {
        $('.sidePlateItem').removeClass("active");
        $('.sideCompanyItem').removeClass("active");
        $('.sideUserItem').removeClass("active");

        $("#category_id").val("");
        $("#dept_id").val("");
        $("#other_user_id").val("");
        queryHistroyInstruct();
    }

    /**
     * 参数切换，target(dom),type(所传类型)
     * @param target
     * @param type
     */
    function switchPrams(target, type, id) {
        if (type === 'user') {
            $('.sideUserItem').removeClass("active");
            $(target).addClass("active");
            $("#other_user_id").val(id);
            //切换用户需要重新加载历史指示
            queryHistroyInstruct();
        } else if (type === 'company') {
            $('.sideCompanyItem').removeClass("active");
            $(target).addClass("active");
            $("#dept_id").val(id);
            //切换公司需要重新加载用户和历史指示
            loadAllUserList(1);
            queryHistroyInstruct();
        } else if (type === 'plate') {
            $('.sidePlateItem').removeClass("active");
            $(target).addClass("active");
            $("#category_id").val(id);
            //切换板块需要重新加载公司、用户、历史指示
            loadAllDeptList(1);
            loadAllUserList(1);
            queryHistroyInstruct();
        }
    }



    /**
     * 板块切换，target(dom))
     * @param target
     */
    function switchPlate(target, type, id) {
        if(type === 'plate') {
            //获取当前返回数组的长度
            var categoryListLength = $("#category_list_length_id").val();
            //判断‘全部’是否被选中
            var isCheckAll = $("#all_plate").hasClass("active");
            //如果当前选中的和数组长度一样且全部被选中，则清除结果集
            if(categoryArr.length == categoryListLength && isCheckAll) {
                categoryArr = [];
            }
            $("#all_plate").removeClass("active");
            //如果已经被选中了，则清除选中
            if($(target).hasClass("active")) {
                $(target).removeClass("active");
                //移除数组中元素
                for(var i in categoryArr) {
                    if(categoryArr[i] == id) {
                        categoryArr.splice(i, 1)
                    }
                }
            } else {
                //当前未被选中，加入元素
                $(target).addClass("active");
                categoryArr.push(id);
            }
        } else {
            $(".plateItem").removeClass("active")
            $("#all_plate").addClass("active");
            //获取全部ID
            categoryArr = [];
            $("[name='plate']").each(function(index,el){
                categoryArr.push($(el)[0].id)
            });
        }
        //执行查询
        loadDeptListByCategoryIds();
    }

    function switchCompany(target, type, id) {
        if(type === 'company') {
            //获取当前返回数组的长度
            var deptListLength = $("#dept_list_length_id").val();
            //判断‘全部’是否被选中
            var isCheckAll = $("#all_company").hasClass("active");
            //如果当前选中的和数组长度一样且全部被选中，则清除结果集
            if(deptArr.length == deptListLength && isCheckAll) {
                deptArr = [];
            }
            $("#all_company").removeClass("active");
            if($(target).hasClass("active")) {
                $(target).removeClass("active");
                for(var i in deptArr) {
                    if(deptArr[i] == id) {
                        deptArr.splice(i, 1)
                    }
                }
            } else {
                $(target).addClass("active");
                deptArr.push(id);
            }
        } else {
            console.log(deptArr)
            $(".companyItem").removeClass("active")
            $("#all_company").addClass("active");
            //获取全部ID
            deptArr = [];
            console.log(deptArr)
            $("[name='company']").each(function(index,el){
                deptArr.push($(el)[0].id)
            });
        }
        loadUserListByCategoryIdsDeptIds();
    }

    function switchUser(target, type, id) {
        if(type === 'user') {
            //获取当前返回数组的长度
            var userListLength = $("#user_list_length_id").val();
            //判断‘全部’是否被选中
            var isCheckAll = $("#all_user").hasClass("active");
            //如果当前选中的和数组长度一样且全部被选中，则清除结果集
            if(userArr.length == userListLength && isCheckAll) {
                userArr = [];
            }
            $("#all_user").removeClass("active");
            if($(target).hasClass("active")) {
                $(target).removeClass("active");
                for(var i in userArr) {
                    if(userArr[i] == id) {
                        userArr.splice(i, 1)
                    }
                }
            } else {
                $(target).addClass("active");
                userArr.push(id);
            }
        } else {
            $(".userItem").removeClass("active")
            $("#all_user").addClass("active");
            //获取全部ID
            userArr = [];
            $("[name='user']").each(function(index,el){
                userArr.push($(el)[0].id)
            });
        }
    }

    /**
     * 时间参数切换，target(dom))
     * @param target
     */
    function switchDate(target, dateType) {
        $('.timeItem').removeClass("active");
        $(target).addClass("active");
        $("#date_type_id").val(dateType);
        queryHistroyInstruct();
    }

    /**
     * 清除水印
     */
    function cleanWaterMark() {
        for(i = 3; i < $("#mask_div00").siblings().length; i++){
            $("#mask_div00").siblings()[i].remove();
            i--
        }
        $("#mask_div00").remove();
    }

    function convertDate(date) {
        var now = new Date(date);
        var year = now.getFullYear();
        var month = now.getMonth() + 1;
        var day = now.getDate();
        return year + "" + (month < 10 ? "0" + month : month) + "" + (day < 10 ? "0" + day : day);
    }


</script>
</body>
</html>
