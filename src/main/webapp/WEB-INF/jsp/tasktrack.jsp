<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0"/>
    <title>工作追踪</title>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css"/>
    <script type="text/javascript" src="/jquery/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/shuiyin.js"></script>
</head>
<body>
<style>
    * {
        padding: 0;
        margin: 0;
        -moz-user-select: none; /*火狐*/
        -webkit-user-select: none; /*webkit浏览器*/
        -ms-user-select: none; /*IE10*/
        -khtml-user-select: none; /*早期浏览器*/
        user-select: none;
    }

    .content {
        width: 100%;
        min-height: 100vh;
        overflow: hidden;
        background-color: #f0f2f9;
        padding: 10px;
    }

    .taskList .taskItem,.allList .item{
        margin-top: 5px;
        background-color: #ffffff;
        min-height: 50px;
        padding: 10px;
    }
    .ell{
        overflow:hidden;
        text-overflow:ellipsis;
        white-space:nowrap;
    }
    .itemTable{
        font-size: 14px;
        table-layout: fixed;
        width:100%;
        border-collapse:separate;
        border-spacing:0px 10px;
    }

</style>

<div class="content">
    <div class="allList">
        <div class="item">
            <span style="color: #171b6d;font-size: 16px"><b>地产--未来方舟</b><button type="button" class="btn btn-danger btn-xs pull-right stateButton">延期</button></span>
            <table class="itemTable">
                <tr onclick="showTime()">
                    <td width="43%" class="ell">当前害得我区的午后的6546548798798</td>
                    <td width="15%" class="td-primary">已完结</td>
                    <td width="42%" class="ell">董事长于2019.2.23指示</td>
                </tr>
                <tr onclick="showTime()">
                    <td width="43%" class="ell">当前害得我区的午后的6546548798798</td>
                    <td width="15%" class="td-primary">已完结</td>
                    <td width="42%" class="ell">董事长于2019.2.23指示</td>
                </tr>
                <tr onclick="showTime()">
                    <td width="43%" class="ell">当前害得后的87987987987987987987</td>
                    <td width="10%" class="td-warning">暂停</td>
                    <td width="42%" class="ell">慕容云海于2019.2.23反馈</td>
                </tr>
                <tr onclick="showTime()">
                    <td width="43%" class="ell">当前害得我区的午后的</td>
                    <td width="10%" class="td-danger">延期</td>
                    <td width="42%" class="ell">张三于2019.2.23反馈</td>
                </tr>
                <tr onclick="showTime()">
                    <td width="43%" class="ell">当前害得我区的午后的</td>
                    <td width="10%" class="td-success">进行中</td>
                    <td width="42%" class="ell">董事长于2019.2.23指示</td>
                </tr>
            </table>
        </div>
    </div>
</div>

<script>

    $(function () {
//        loadTaskTrackList();
    });

    /**
     * 加载任务追踪列表
     */
    function loadTaskTrackList() {
        $.post("findAllCategory", null, function (categoryList) {
            console.log(categoryList)
        });
    }

</script>
</body>
</html>
