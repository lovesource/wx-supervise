<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0"/>
    <title>工作看板</title>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css"/>
    <script type="text/javascript" src="/jquery/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/shuiyin.js"></script>
</head>

<body>
<style>
    *{
        padding: 0;
        margin: 0;
        -moz-user-select:none;/*火狐*/
        -webkit-user-select:none;/*webkit浏览器*/
        -ms-user-select:none;/*IE10*/
        -khtml-user-select:none;/*早期浏览器*/
        user-select:none;
    }
    .content{
        width: 100%;
        min-height: 100vh;
        overflow: hidden;
        background-color: #f0f2f9;
        padding: 10px;
    }
    .plate{
        width: 100%;
        padding: 10px;
        min-height: 50px;
        background-color: #ffffff;
    }
    .plateItem {
        float: left;
        min-width: 40px;
        height: 30px;
        font-size: 14px;
        line-height: 30px;
        margin-left: 4px;
        text-align: center;
        color: #07349a;
        cursor: pointer;
    }
    .plateItem.active{
        border-bottom: 1px solid #ee3148;
        color: #ee3148;
    }

    .plateItem.selected{
        color: #747474;
        cursor: pointer;
    }

    .projectItem , .userItem{
        float: left;
        min-width: 50px;
        height: 26px;
        font-size: 12px;
        line-height: 26px;
        margin-left: 6px;
        margin-top: 6px;
        text-align: center;
        padding: 0px 5px;
        background-color: #f2f2f2;
        border-radius: 8px;
    }
    .projectItem.active,.userItem.active{
        background-color: #ffffff;
        border: 1px solid #07349a;
        color: #07349a;
    }

    .taskList .taskItem,.allList .item{
        margin-top: 5px;
        background-color: #ffffff;
        min-height: 50px;
        padding: 10px;
    }
    .taskList .taskItem button{
        width: 80px;
        margin-top: -10px;
    }
    .taskList .taskItem p{
        padding-top: 10px;
        font-size: 12px;
        color: #666666;
        word-break: break-all;
    }
    .taskList .taskItem .footer{
        font-size: 12px;
        padding-top: 10px;
        color: #747474;
    }
    .modal-dialog{
        margin: 0px;
    }

    /*Right*/
    .modal.right.fade .modal-dialog {
        right: -320px;
        -webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
        -moz-transition: opacity 0.3s linear, right 0.3s ease-out;
        -o-transition: opacity 0.3s linear, right 0.3s ease-out;
        transition: opacity 0.3s linear, right 0.3s ease-out;
    }

    .modal.right.fade.in .modal-dialog {
        right: 0;
    }

    .itemTable{
        font-size: 14px;
        table-layout: fixed;
        width:100%;
        border-collapse:separate;
        border-spacing:0px 10px;
    }
    .td-primary{
        color: #337ab7;
    }
    .td-warning{
        color: #f0ad4e;
    }
    .td-danger{
        color: #d9534f;
    }
    .td-success{
        color: #5cb85c;
    }

    .ell{
        overflow:hidden;
        text-overflow:ellipsis;
        white-space:nowrap;
    }

</style>
<div class="content">
    <div class="plate">
        <div class="plateItem active" onclick="switchPlate(this, '')">全部</div>
        <c:forEach items="${categoryList}" var="item">
            <div class="plateItem" onclick="switchPlate(this, '${item.categoryId}')">${item.categoryName}</div>
        </c:forEach>
        <div id='selectedId' class='plateItem'  style="float: right" data-toggle='modal' data-target='#myModal'>筛选 <span class='glyphicon glyphicon-filter' aria-hidden='true'></span></div>
        <div style="clear: both"></div>
    </div>
    <div class="modal right fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="margin-left: 100px;">
            <div class="modal-content" style="min-height: 100vh">
                <div class="modal-header">
                    <p style="background-color: #D9534F; color: #FFF">公司</p>
                    <%--<p><button type="button" class="btn btn-danger" style="color: #FFF">公司</button></p>--%>
                    <div id="deptListId"></div>
                    <%--<c:forEach items="${deptList}" var="item">--%>
                    <%--<div class="projectItem" onclick="switchPrams(this,'project', '${item.deptId}')">${item.deptName}</div>--%>
                    <%--</c:forEach>--%>
                    <div style="clear: both"></div>
                </div>
                <div class="modal-body">
                    <p style="background-color: #D9534F; color: #FFF">目标责任人</p>
                    <div id="userListId"></div>
                    <%--<c:forEach items="${deptList}" var="item">--%>
                    <%--<div class="userItem" onclick="switchPrams(this,'project', '${item.deptId}')">${item.deptName}</div>--%>
                    <%--</c:forEach>--%>
                    <div style="clear: both"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" style="width: 90px "  onclick="reset()">重置</button>
                    <button type="button" class="btn btn-primary" style="width: 90px " data-dismiss="modal">确认</button>
                </div>
            </div>
        </div>
    </div>
    <div class="taskList" id="taskListId" style="display: none">
    </div>

    <div class="allList" id="allList">
        <%--<div class="item">--%>
        <%--<span style="color: #171b6d;font-size: 16px"><b>地产--未来方舟</b></span>--%>
        <%--<table class="itemTable" >--%>
        <%--<tr onclick="showTime()">--%>
        <%--<td width="43%" class="ell">当前害得我区的午后的6546548798798</td>--%>
        <%--<td width="15%" class="td-primary">已完结</td>--%>
        <%--<td width="42%" class="ell">董事长于2019.2.23指示</td>--%>
        <%--</tr>--%>
        <%--<tr onclick="showTime()">--%>
        <%--<td width="43%" class="ell">当前害得我区的午后的6546548798798</td>--%>
        <%--<td width="15%" class="td-primary">已完结</td>--%>
        <%--<td width="42%" class="ell">董事长于2019.2.23指示</td>--%>
        <%--</tr>--%>
        <%--<tr onclick="showTime()">--%>
        <%--<td width="43%" class="ell">当前害得后的87987987987987987987</td>--%>
        <%--<td width="10%" class="td-warning">暂停</td>--%>
        <%--<td width="42%" class="ell">慕容云海于2019.2.23反馈</td>--%>
        <%--</tr>--%>
        <%--<tr onclick="showTime()">--%>
        <%--<td width="43%" class="ell">当前害得我区的午后的</td>--%>
        <%--<td width="10%" class="td-danger">延期</td>--%>
        <%--<td width="42%" class="ell">张三于2019.2.23反馈</td>--%>
        <%--</tr>--%>
        <%--<tr onclick="showTime()">--%>
        <%--<td width="43%" class="ell">当前害得我区的午后的</td>--%>
        <%--<td width="10%" class="td-success">进行中</td>--%>
        <%--<td width="42%" class="ell">董事长于2019.2.23指示</td>--%>
        <%--</tr>--%>
        <%--</table>--%>
        <%--</div>--%>
    </div>

    <input type="hidden" id="category_id" value=""/>
    <input type="hidden" id="dept_id" value=""/>
    <input type="hidden" id="other_user_id" value=""/>
</div>

<script>


    $(function(){
        //0、加载首页任务列表
        loadAllTaskList();
        //1、加载工作信息列表
//        loadTaskCategoryList();
        //2、加载公司信息列表
        loadDeptList();
        //3、加载人员列表
        loadUserList();
    });

    function convertDate(date) {
        var now = new Date(date);
        var year = now.getFullYear();
        var month = now.getMonth() + 1;
        var day = now.getDate();
        return year + "" + (month < 10 ? "0" + month : month) + "" + (day < 10 ? "0" + day : day);
    }

    /**
     * 获取参数
     */
    function getQueryParams() {
        var categoryId = $("#category_id").val();
        var deptId = $("#dept_id").val();
        var otherUserId = $("#other_user_id").val();
        var params = {"categoryId": categoryId, "deptId": deptId, "otherUserId": otherUserId};
        return params;
    }


    /**
     * 页面初始化加载板块与工作列表
     */
    function loadTaskCategoryList() {
        var params = getQueryParams();
        $.post("findTaskAndCategoryList", params, function(resultList){
            console.log(resultList)
            $("#taskListId").html(dealWithResultList(resultList));
        });
    }

    function loadAllTaskList() {
        var deptId = $("#dept_id").val();
        var otherUserId = $("#other_user_id").val();
        var params = {"deptId": deptId, "otherUserId": otherUserId};
        $.post("findTaskAndNewestFeedback", params, function(resultList){
            console.log(resultList)
            $("#allList").html(dealWithAllTaskResultList(resultList));
        });
    }

    /**
     * 页面加载公司信息列表
     */
    function loadDeptList() {
        var params = getQueryParams();
        $.post("findDeptList", params, function(deptList){
            console.log(deptList)
            $("#deptListId").html(dealWithDeptResultList(deptList));
        });
    }

    /**
     * 页面加载用户信息列表
     */
    function loadUserList() {
        var params = getQueryParams();
//        console.log("load user list....")
//        console.log(params)
        $.post("findUserList", params, function(userList){
            $("#userListId").html(dealWithUserResultList(userList));
        });
    }


    function dealWithResultList(resultList) {
        cleanWaterMark();
        var divHtml = "";
        for(var i in resultList) {
            divHtml += '<div class="taskItem" onclick=goDetailPage("' + resultList[i].taskId + '")>' +
                '<span style="color: #171b6d;font-size: 18px">' + resultList[i].category.categoryName + '</span>' +
                '<span style="padding-left: 10px">' + resultList[i].dept.deptName + '</span>';
            if(resultList[i].status == 'D') {
                divHtml += "<button type='button' class='btn btn-danger btn-xs pull-right'>延期</button>";
            } else if(resultList[i].status == 'P') {
                divHtml += "<button type='button' class='btn btn-warning btn-xs pull-right'>暂停</button>";
            } else if(resultList[i].status == 'F') {
                divHtml += "<button type='button' class='btn btn-primary btn-xs pull-right'>已完结</button>";
            } else if(resultList[i].status == 'G') {
                divHtml += "<button type='button' class='btn btn-success btn-xs pull-right'>进行中</button>";
            }
            divHtml += "<p style='font-size: 14px; color: #000; font-weight: bold;'>" + resultList[i].taskName + "</p>";
            divHtml += "<p>" + resultList[i].taskContent + "</p>";
            divHtml += "<div class='footer'><span>开始时间:" + (resultList[i].startTime == null ? '' : resultList[i].startTime) + "</span><span class='pull-right'>责任人:";
            var index = 0;
            for(var j in resultList[i].userVoList) {
                if (resultList[i].userVoList[j].userTask.role == '4') {
                    divHtml += "&nbsp;" + resultList[i].userVoList[j].userName + "&nbsp;";
                    if(index == 2) {
                        divHtml += "...";
                        break;
                    }
                    index++;
                }
            }
            divHtml += "</span></div>";
            divHtml += '<p>计划完成时间：' + (resultList[i].closeTime == null ? '' : resultList[i].closeTime) + '</p>';
            divHtml += "</div>";
        }
        $.post("findUserName", null, function(user){
            var text = user.userName + " " + convertDate(new Date());
            setTimeout(watermarkCanvas({ watermark_txt: text }),1000);
        });
        return divHtml;
    }


    function dealWithAllTaskResultList(resultList) {
        cleanWaterMark();
        var divHtml = "";
        for(var i in resultList) {
            for(var k in resultList[i].deptList) {
                divHtml += '<div class="item">';
                divHtml += '<span style="color: #171b6d;font-size: 16px"><b>' + resultList[i].categoryName + '--' + resultList[i].deptList[k].deptName + '</b></span>';
                divHtml += '<table class="itemTable" >';
                for(var j in resultList[i].taskList) {
                    if(resultList[i].taskList[j].deptId == resultList[i].deptList[k].deptId) {
                        divHtml += '<tr onclick=goDetailPage("' + resultList[i].taskList[j].taskId + '")>';
                        divHtml += '<td width="35%" class="ell">' + resultList[i].taskList[j].taskName + '</td>';
                        if(resultList[i].taskList[j].status == 'D') {
                            divHtml += '<td width="15%" class="td-danger">延期</td>';
                        } else if(resultList[i].taskList[j].status == 'P') {
                            divHtml += '<td width="15%" class="td-warning">暂停</td>';
                        } else if(resultList[i].taskList[j].status == 'F') {
                            divHtml += '<td width="15%" class="td-primary">已完结</td>';
                        } else if(resultList[i].taskList[j].status == 'G') {
                            divHtml += '<td width="15%" class="td-success">进行中</td>';
                        }
                        if(resultList[i].feedbackVOList[j].feedbackTime != null) {
                            if(resultList[i].feedbackVOList[j].userTask.role == '1') {
                                divHtml += '<td width="50%" class="ell">董事长于' + resultList[i].feedbackVOList[j].feedbackTime + '指示</td>';
                            } else {
                                divHtml += '<td width="50%" class="ell">' + resultList[i].feedbackVOList[j].user.userName + '于' + resultList[i].feedbackVOList[j].feedbackTime + '反馈</td>';
                            }
                        } else {
                            divHtml += '<td width="50%" class="ell">无反馈信息</td>';
                        }

                        divHtml += '</tr>';
                    }
                }
                divHtml += '</table>';
                divHtml += '</div>';
            }
        }


        $.post("findUserName", null, function(user){
            var text = user.userName + " " + convertDate(new Date());
            setTimeout(watermarkCanvas({ watermark_txt: text }),1000);
        });
        return divHtml;
    }


    function dealWithDeptResultList(resultList) {
        var divHtml = "";
        for(var i in resultList) {
            divHtml += '<div>'
            divHtml += '<p style="font-size: 12px; margin-bottom: -2px;color: #171b6d"><b>&nbsp;&nbsp;&nbsp;' + resultList[i].categoryName + '</b></p>';
            for(var j in resultList[i].deptList) {
                divHtml += '<div class="projectItem" onclick="switchPrams(this, \'project\', \'' + resultList[i].deptList[j].deptId + '\')">' + resultList[i].deptList[j].deptName + '</div>';
            }
            divHtml += '<div style="clear: both"></div></div>'
        }
        return divHtml;
    }

    function dealWithUserResultList(userList) {
        var divHtml = "";
        for(var i in userList) {
            divHtml += '<div class="userItem" onclick="switchPrams(this, \'user\', \'' + userList[i].userId + '\')">' + userList[i].userName + '</div>';
        }
        return divHtml;
    }

    function goDetailPage(taskId) {
        window.location.href = "detail?taskId="+taskId;
    }

    /**
     * 重置清空
     */
    function reset() {
        $('.projectItem').removeClass("active");
        $('.userItem').removeClass("active");
        $("#dept_id").val("");
        $("#other_user_id").val("");
        var categoryId = $("#category_id").val();
        if(categoryId == '') {
            loadAllTaskList();
        } else {
            loadTaskCategoryList();
        }
    }

    /**
     * 参数切换，target(dom),type(所传类型)
     * @param target
     * @param type
     */
    function switchPrams(target,type, id) {
        var categoryId = $("#category_id").val();
        if(type === 'user'){
            $('.userItem').removeClass("active");
            $(target).addClass("active");
            $("#other_user_id").val(id);

            //这里为了区分首页刷新页面的条件
            //刷新首页
            if(categoryId == '') {
                loadAllTaskList();
            } else {
                //切换用户需要重新加载工作
                loadTaskCategoryList();
            }

        }else if(type === 'project'){
            $('.projectItem').removeClass("active");
            $(target).addClass("active");
            $("#dept_id").val(id);
            //这里为了区分首页刷新页面的条件
            //刷新首页
            if(categoryId == '') {
                loadAllTaskList();
            } else {
                //切换公司需要重新加载用户和工作
                loadTaskCategoryList();
            }
            loadUserList();
        }
    }

    /**
     * 板块参数切换，target(dom),type(所传类型)
     * @param target
     */
    function switchPlate(target, categoryId) {
        //保存当前点击的板块ID
        $("#category_id").val(categoryId);
        var targetTextStr = target.innerText;
        if(targetTextStr === '全部'){
            $("#taskListId").hide();
            $("#allList").show();
        }else {
            $("#allList").hide();
            $("#taskListId").show();
        }

        $('.plateItem').removeClass("active");
        $(target).addClass("active");
        //切换板块时，清除选中的公司和人员条件
        $("#dept_id").val("");
        $("#other_user_id").val("");

        var categoryId = $("#category_id").val();
        if(categoryId == '') {
            //重新加载工作
            loadAllTaskList();
        } else {
            loadTaskCategoryList();
        }
        //切换板块需要加载公司
        loadDeptList();
        //切换板块需要重新加载用户
        loadUserList();
    }

    /**
     * 清除水印
     */
    function cleanWaterMark() {
        for(i = 3; i < $("#mask_div00").siblings().length; i++){
            $("#mask_div00").siblings()[i].remove();
            i--
        }
        $("#mask_div00").remove();
    }

</script>
</body>
</html>